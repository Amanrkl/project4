
// Function similar to find function which return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test


function filter(elements, cb) {
    let result=[]
    if ( !(elements === undefined) && Array.isArray(elements) && typeof cb === 'function') {
        let length=elements.length
        for (let index = 0 ; index < length; index++) {
            if (cb(elements[index], index, elements) === true){
                result.push(elements[index]);
            }
        }   
    }
    return result
}


module.exports = filter;