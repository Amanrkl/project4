
// Function similar to Foreach function which iterates over a list of elements, yielding each in turn to the `cb` function.
// Constraints - only needs to work with arrays and  also pass the index into `cb` as the second argument


function each(elements, cb) {
    if ( !(elements === undefined) && Array.isArray(elements) && typeof cb === 'function') {
        let length=elements.length
        for (let index = 0 ; index < length; index++) {
            cb(elements[index], index, elements);
        }   
    }
}


module.exports = each;