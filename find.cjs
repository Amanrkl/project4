
// Function similar to find function which Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.


function find(elements, cb) {
    if ( !(elements === undefined) && Array.isArray(elements) && typeof cb === 'function') {
        let length=elements.length
        for (let index = 0 ; index < length; index++) {
            if (cb(elements[index], index, elements)){
                return elements[index];
            }
        }   
    }
    return 
}


module.exports = find;