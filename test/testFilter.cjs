const filter = require('../filter.cjs')

const items = [1, 2, 3, 4, 5, 5];
const isEven = function(num){ return num % 2 == 0; }
const isMultipleof6 = function(num){ return num % 6 == 0; }
console.log(filter(items,isEven));
console.log(filter(items,isMultipleof6));