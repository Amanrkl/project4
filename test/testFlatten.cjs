const flatten = require('../flatten.cjs')

const nestedArray1 = [1, [2,4], [[3]], [[[4]]]];
const nestedArray2 = [1, [2,4], [[3]], [[[4]]]];
const nestedArray3 = [1, , [2, ,4], [[3, ,6]], [[[4]]]];
const nestedArray4 = [1, [2, ,5,6], [[3]], [[[4, ,8]]]];
const items = [1, 2, [3], 4, 5, 5];
console.log(flatten(nestedArray1,2));
console.log(flatten(nestedArray2));
console.log(flatten(nestedArray3,1));
console.log(flatten(nestedArray4));
console.log(flatten(items));