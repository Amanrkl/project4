
// Function similar to Map function which calls a provided callback function once for each element in an array, in order, and function creates a new array from the res .

function map(elements, cb) {
    let mappedElements=[]
    if ( !(elements === undefined) && Array.isArray(elements) && typeof cb === 'function') {
        let length=elements.length
        for (let index = 0 ; index < length; index++) {
            mappedElements.push(cb(elements[index], index, elements));
        }   
    }
    return mappedElements;
}

module.exports = map;