
// Function similar to reduce function which combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

function reduce(elements, cb, startingValue) {
    
    if ( !(elements === undefined) && Array.isArray(elements) && typeof cb === 'function') {
        let index = 0
        if (!startingValue) {
            startingValue = elements[0];
            index ++;
        }
            
        let length=elements.length
        for (; index < length; index++) {
            startingValue =  cb(startingValue, elements[index], index, elements);
        }   
    }
    return startingValue;
}

module.exports = reduce;