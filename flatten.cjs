
// Function similar to Foreach function which iterates over a list of elements, yielding each in turn to the `cb` function.
// Constraints - only needs to work with arrays and  also pass the index into `cb` as the second argument


function flatten(array,depth) {
    if (!depth && depth !== 0) {
        depth = 1;
    }
    let flatArray = [];
    for (let index = 0; index < array.length; index++) {
        let value= array[index]
        if (Array.isArray(array[index])) {
            if (depth > 1) {
            flatArray.push(...flatten(array[index],depth-1));}
            else{
                let nestedIndex = 0, len = value.length;
                while (nestedIndex < len){
                    if (value[nestedIndex] !== undefined){
                        flatArray.push(value[nestedIndex]);}
                    nestedIndex++;
                    }
            }
            
        } else if (array[index] !== undefined){
            flatArray.push(array[index]);
        }
    }
    return flatArray;
}
  

module.exports = flatten;